ARG VERSION=latest

FROM selenium/node-firefox:${VERSION}

COPY install.sh /root/install.sh

USER root

RUN /bin/bash /root/install.sh

USER seluser
