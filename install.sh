#!/bin/bash
# Stop on error
set -e
# Install mandatory programs and utilities
export DEBIAN_FRONTEND=noninteractive
apt-get --yes --quiet=2 update
apt-get --yes --quiet=2 install \
	ca-certificates \
	curl \
	gcc \
	g++ \
	make \
	tar
curl -sL https://deb.nodesource.com/setup_12.x | bash -
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
apt-get --yes --quiet=2 update
apt-get install -y nodejs yarn
apt-get clean
rm -rf /var/lib/apt/lists/*
